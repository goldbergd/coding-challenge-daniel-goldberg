#
# This file has the method to print raster data of the given file
#   https://www.gdal.org/gdal_tutorial.html
#
from osgeo import gdal

def print_raster_info(gdal_raster):
    print("Driver: {}/{}".format(gdal_raster.GetDriver().ShortName,
                                 gdal_raster.GetDriver().LongName))
    print("Size is {} x {} x {}".format(gdal_raster.RasterXSize,
                                        gdal_raster.RasterYSize,
                                        gdal_raster.RasterCount))

    print("Projection is {}".format(gdal_raster.GetProjection()))
    geotransform = gdal_raster.GetGeoTransform()
    if geotransform:
        print("Origin = ({}, {})".format(geotransform[0], geotransform[3]))
        print("Pixel Size = ({}, {})".format(geotransform[1], geotransform[5]))

    # 1 band in this data
    band = gdal_raster.GetRasterBand(1)
    print("Band Type={}".format(gdal.GetDataTypeName(band.DataType)))

    min = band.GetMinimum()
    max = band.GetMaximum()
    if not min or not max:
        (min,max) = band.ComputeRasterMinMax(True)
        print("Need to calculate")
    print("Min={:.3f}, Max={:.3f}".format(min,max))

    if band.GetOverviewCount() > 0:
        print("Band has {} overviews".format(band.GetOverviewCount()))

    if band.GetRasterColorTable():
        print("Band has a color table with {} entries".format(band.GetRasterColorTable().GetCount()))
