# This assumes that the data is already geospatially registered
#
# https://gis.stackexchange.com/questions/28966/python-gdal-package-missing-header-file-when-installing-via-pip
# sudo apt-get install libgdal-dev
# sudo -H CPATH=$CPATH:/usr/include/gdal/ pip install GDAL==$(gdal-config --version | awk -F'[.]' '{print $1"."$2}')
from osgeo import gdal
from print_raster_info import print_raster_info
import numpy as np
import sys


def get_largest_factor(starting_value, number):
    for r in range(starting_value, 0, -1):
        if (number % r) == 0:
            return r


def count_density_and_loss(density, loss):
    out = {}
    lt_half = np.less(density, 50)
    gte_half = np.logical_not(lt_half)
    # From documentation:
    #   2001-2018, with values 1-18 ...
    #   Ignore areas with no loss.
    for year in range(2001, 2018 + 1):
        loss_for_year = np.equal(loss, year - 2000)
        # count_nonzero tended to have a faster impl on my machine than sum
        lt_count = np.count_nonzero(np.logical_and(loss_for_year, lt_half))
        gte_count = np.count_nonzero(np.logical_and(loss_for_year, gte_half))
        out[year] = np.array([lt_count, gte_count])
    return out


if __name__ == '__main__':
    out_file_name = 'out.csv'
    if len(sys.argv) > 1:
        out_file_name = sys.argv[1]

    density_file = '/home/goldbergd/Downloads/tcd_2000_10N_020E.tif'
    loss_file = '/home/goldbergd/Downloads/loss_v1_6_10N_020E.tif'
    if len(sys.argv) > 3:
        density_file = sys.argv[2]
        loss_file = sys.argv[3]

    starting_rows = 4000
    if len(sys.argv) > 4:
        starting_rows = int(sys.argv[4])

    print('Using')
    print('  output file name: ' + out_file_name)
    print('  input density file name: ' + density_file)
    print('  input loss file name: ' + loss_file)
    print('  desired number of rows to use at once: ', starting_rows)

    density_gdal = gdal.Open(density_file, gdal.GA_ReadOnly)
    loss_gdal = gdal.Open(loss_file, gdal.GA_ReadOnly)

    # MUST GO EVENLY INTO RASTER ROWS
    rows_at_once = get_largest_factor(starting_rows, density_gdal.RasterYSize)
    print('Using: ', rows_at_once, ' rows at once')

    # Read rows_at_once rows of the image
    # compute its portions' metrics
    # Combine with the total image metrics
    output_map = {}
    for row in range(0, density_gdal.RasterXSize, rows_at_once):
        # Read
        den = np.array(density_gdal.GetRasterBand(1).ReadAsArray(xoff=0, yoff=row,
                                                                 win_xsize=density_gdal.RasterXSize, win_ysize=rows_at_once))
        loss = np.array(loss_gdal.GetRasterBand(1).ReadAsArray(xoff=0, yoff=row,
                                                               win_xsize=loss_gdal.RasterXSize, win_ysize=rows_at_once))
        # compute metrics
        row_map = count_density_and_loss(den, loss)
        # Aggregate recent portion of raster with the rest
        # Able to do this since the operation is linear (addition)
        for key in row_map:
            if key in output_map:
                output_map[key] += row_map[key]
            else:
                output_map[key] = row_map[key]
    with open(out_file_name, 'w') as out_file:
        out_file.write('year, tcd_0_49, tcd_50_100\n')
        for year in sorted(output_map):
            line = str(year) + ', ' + \
                str(output_map[year][0]) + ', ' + str(output_map[year][1])
            out_file.write(line + '\n')
            print(line)

    print('\ndone!\n')
