# Coding Challenge: Data Engineer 

## Option 1: Raster Analysis

Below are links to two raster datasets we use frequently in Global Forest Watch. 

The values of the pixels in the tree cover loss raster represent the year during which tree cover loss was detected in that pixel (2001-2018, with values 1-18). Pixel value 0 represents no loss during that period. You can learn more about the tree cover loss raster here: https://data.globalforestwatch.org/datasets/63f9425c45404c36a23495ed7bef1314

Pixel values of the Tree Cover Density dataset represent the percent of the pixel with tree canopy cover. You can learn more about this data here: https://data.globalforestwatch.org/items/7876b225f8034a0ebba79fad4afb80ad

Group tree cover density into two classes: <50% and >= 50%. 
Afterwards, overlay both datasets and calculate the number of tree cover loss pixels per year and tree cover density class. Ignore areas with no loss.  

Write the results into a CSV table with the following structure:

|year|tcd_0_49|tcd_50_100|
|----|--------|----------|
|2001|....||
|2002|....||
|2003|....||
|....|||

### Data
Tree Cover Loss: https://gfw-files.s3.amazonaws.com/coding-challenge/data-engineer/loss_v1_6_10N_020E.tif

Tree Cover Density: https://gfw-files.s3.amazonaws.com/coding-challenge/data-engineer/tcd_2000_10N_020E.tif

### Requirements
Write code using either Python or Scala only.

Use open source packages and libraries only. 

### Bonus Points
Create Dockerfile which includes all dependencies and allow to run application inside Docker container. 


## Option 2: Vector manipulation
Inject the GeoJSON file below into a PostgreSQL database.

Create a 10 x 10 degree vector grid (aka Fishnet) inside PostgreSQL and tile geometries using the grid.

Preserve all columns from original GeoJSON and add row and column index of grid to final dataset.

### Data
GeoJSON: https://gfw-files.s3.amazonaws.com/coding-challenge/data-engineer/us_states_20m.json

### Requirements

Write code using either Bash or Python together with SQL. 

Use open source packages and libraries only. 

### Bonus Points
Create Dockerfile which includes all dependencies and allow to run application inside Docker container. 

## How to submit (either option)?
Clone this repo, and submit your code with a pull request.
